package blog.service;

import blog.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    User currentUser();
    User userAuthentication();
}
