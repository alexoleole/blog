package blog.bindingModel;

import blog.entity.Category;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public class GalleryBindingModel {
    @NotNull
    private String title;

    private String info;

    private MultipartFile image;

    private Integer categoryId;

    private String tagString;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getTagString() {
        return tagString;
    }

    public void setTagString(String tagString) {
        this.tagString = tagString;
    }
}
