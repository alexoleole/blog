package blog.bindingModel;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;

public class CategoryBindingModel {
    private String name;
    private MultipartFile catPicture;

    public MultipartFile getCatPicture() {
        return catPicture;
    }

    public void setCatPicture(MultipartFile catPicture) {
        this.catPicture = catPicture;
    }

    public String getName() {return name;}
    public void setName(String name) {this.name=name;}
}
