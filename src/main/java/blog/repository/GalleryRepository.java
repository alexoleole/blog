package blog.repository;

import blog.entity.Gallery;
import blog.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GalleryRepository extends JpaRepository <Gallery,Integer> {
    /*@Query(value = "SELECT t1.id,t1.image,t1.info,t1.title,t1.author_id,t1.category_id FROM galleries t1 WHERE t1.category_id = :id", nativeQuery = true)
    List<Gallery> findAllByCategoryId(@Param("id") Integer id);*/
}
