package blog.entity;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
public class Category {
    private Integer id;

    private String name;

    private String catPicture;

    private Set<Gallery> galleries;


    public Category() {
    }

    public Category(String name) {
        this.name = name;
        this.galleries = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId () {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    @Column(unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(columnDefinition = "text")
    public String getCatPicture() {
        return catPicture;
    }

    public void setCatPicture(String catPicture) {
        this.catPicture = catPicture;
    }

    @OneToMany(mappedBy = "category")
    public Set<Gallery> getGalleries() {
        return galleries;
    }

    public void setGalleries(Set<Gallery> galleries) {
        this.galleries = galleries;
    }

}
