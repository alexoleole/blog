package blog.entity;

import javax.persistence.*;

@Entity
@Table(name = "pictures")
public class Picture {
    private Integer id;
    private String pictures;
    private Article article;

    public Picture(String pictures, Article article) {
        this.pictures = pictures;
        this.article = article;
    }

    public Picture() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(columnDefinition = "text")
    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    @ManyToOne
    @JoinColumn(name = "articleId")
    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
