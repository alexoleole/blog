package blog.viewModels;

import java.util.List;

public class GalleryViewModel {
    private Integer id;
    private String title;
    private String info;
    private String authorName;
    private String image;
    private Integer categoryId;
    private List<TagViewModel> tags;

    public GalleryViewModel(Integer id, String title, String info, String authorName, String image, Integer categoryId, List<TagViewModel> tags) {
        this.id = id;
        this.title = title;
        this.info = info;
        this.authorName = authorName;
        this.image = image;
        this.categoryId = categoryId;
        this.tags = tags;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public List<TagViewModel> getTags() {
        return tags;
    }

    public void setTags(List<TagViewModel> tags) {
        this.tags = tags;
    }
}
