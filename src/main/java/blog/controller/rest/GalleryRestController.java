package blog.controller.rest;

import blog.entity.Gallery;
import blog.entity.Tag;
import blog.repository.GalleryRepository;
import blog.viewModels.GalleryViewModel;
import blog.viewModels.TagViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class GalleryRestController {

    @Autowired
    private GalleryRepository galleryRepository;

    @PostMapping("/search")
    public List<GalleryViewModel> search(){

        List<Gallery> galleries = this.galleryRepository.findAll();
        List<GalleryViewModel> galleryViewModels = new ArrayList<>();


        for(Gallery gallery : galleries){

            List<TagViewModel> tags = new ArrayList<>();

            for(Tag tag : gallery.getTags()){
                tags.add(new TagViewModel(
                        tag.getName(),
                        tag.getId()
                ));
            }

            galleryViewModels.add(new GalleryViewModel(
                    gallery.getId(),
                    gallery.getTitle(),
                    gallery.getInfo(),
                    gallery.getAuthor().getFirstName() + " " + gallery.getAuthor().getLastName(),
                    gallery.getImage(),
                    gallery.getCategory().getId(),
                    tags
            ));
        }

        return galleryViewModels;
    }


}