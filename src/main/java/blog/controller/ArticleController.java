package blog.controller;

import blog.bindingModel.ArticleBindingModel;
import blog.entity.Article;
import blog.entity.Picture;
import blog.entity.Tag;
import blog.entity.User;
import blog.repository.ArticleRepository;
import blog.repository.PictureRepository;
import blog.repository.TagRepository;
import blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ArticleController {
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private PictureRepository pictureRepository;

    private final UserService userService;

    @Autowired
    public ArticleController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/article/create")
    @PreAuthorize("isAuthenticated()")
    public String create(Model model) {

        List<Article> articles = this.articleRepository.findAll();

        model.addAttribute("user", this.userService.userAuthentication());
        model.addAttribute("view", "article/create");
        model.addAttribute("article", articles);

        return "base-layout";
    }

    @PostMapping("/article/create")
    @PreAuthorize("isAuthenticated()")
    public String createProcess(ArticleBindingModel articleBindingModel) throws IOException {

        User entityUser = this.userService.currentUser();
        HashSet<Tag> tags = this.findTagsFromString(articleBindingModel.getTagString());

        Article articleEntity = new Article(
                articleBindingModel.getTitle(),
                articleBindingModel.getContent(),
                entityUser,
                tags
        );

        Article article = this.articleRepository.saveAndFlush(articleEntity);

        for (MultipartFile file : articleBindingModel.getPictures()) {
            String encoded = Base64.getEncoder().encodeToString(file.getBytes());
            Picture picture = new Picture(encoded, articleEntity);
            Picture pictureEntity = this.pictureRepository.saveAndFlush(picture);
            articleEntity.addPicture(pictureEntity);
        }

        this.articleRepository.saveAndFlush(article);
        return "redirect:/";
    }

    @GetMapping("/article/{id}")
    public String details(Model model, @PathVariable Integer id) {
        if (!this.articleRepository.exists(id)) {
            return "redirect:/";
        }

        Article article = this.articleRepository.findOne(id);


        model.addAttribute("user", this.userService.userAuthentication());
        model.addAttribute("article", article);
        model.addAttribute("view", "article/details");


        return "base-layout";
    }

    @PostMapping("/article/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String deleteProcess(@PathVariable Integer id) {
        if (!this.articleRepository.exists(id)) {
            return "redirect:/";
        }

        Article article = this.articleRepository.findOne(id);

        if (!isUserAuthorOrAdmin(article)) {
            return "redirect:/article/" + id;
        }

        for (Picture picture : article.getPictures()) {
            this.pictureRepository.delete(picture);
        }

        this.articleRepository.delete(article);

        return "redirect:/";
    }

    @GetMapping("/article/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String edit(@PathVariable Integer id, Model model) {
        if (!this.articleRepository.exists(id)) {
            return "redirect:/";
        }

        Article article = this.articleRepository.findOne(id);


        if (!isUserAuthorOrAdmin(article)) {
            return "redirect:/article/" + id;
        }

        String tagString = article.getTags().stream()
                .map(Tag::getName)
                .collect(Collectors.joining(", "));

        model.addAttribute("user", this.userService.userAuthentication());
        model.addAttribute("view", "article/edit");
        model.addAttribute("article", article);
        model.addAttribute("tags", tagString);


        return "base-layout";
    }

    @PostMapping("/article/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String editProcess(@PathVariable Integer id, ArticleBindingModel articleBindingModel) throws IOException {
        if (!this.articleRepository.exists(id)) {
            return "redirect:/";
        }
        Article article = this.articleRepository.findOne(id);

        if (!isUserAuthorOrAdmin(article)) {
            return "redirect:/article/" + id;
        }

        HashSet<Tag> tags = this.findTagsFromString(articleBindingModel.getTagString());

        article.setContent(articleBindingModel.getContent());
        article.setTitle(articleBindingModel.getTitle());
        article.setTags(tags);



        this.articleRepository.saveAndFlush(article);
        return "redirect:/article/" + article.getId();
    }

    @GetMapping("/article/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String delete(Model model, @PathVariable Integer id) {
        if (!this.articleRepository.exists(id)) {
            return "redirect:/";
        }
        Article article = this.articleRepository.findOne(id);

        if (!isUserAuthorOrAdmin(article)) {
            return "redirect:/article/" + id;
        }

        model.addAttribute("user", this.userService.userAuthentication());
        model.addAttribute("article", article);
        model.addAttribute("view", "article/delete");

        return "base-layout";

    }

    private boolean isUserAuthorOrAdmin(Article article) {
        User userEntity = this.userService.currentUser();

        return userEntity.isAdmin() || userEntity.isAuthor(article);
    }

    private HashSet<Tag> findTagsFromString(String tagString) {
        HashSet<Tag> tags = new HashSet<>();
        String[] tagNames = tagString.split(",\\s");

        for (String tagName : tagNames) {
            Tag currentTag = this.tagRepository.findByName(tagName);

            if (currentTag == null) {
                currentTag = new Tag(tagName);
                this.tagRepository.saveAndFlush(currentTag);
            }

            tags.add(currentTag);
        }
        return tags;
    }


}

