package blog.controller;

import blog.bindingModel.GalleryBindingModel;
import blog.entity.Category;
import blog.entity.Gallery;
import blog.entity.Tag;
import blog.entity.User;
import blog.repository.CategoryRepository;
import blog.repository.GalleryRepository;
import blog.repository.TagRepository;
import blog.repository.UserRepository;
import blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;


@Controller
public class GalleryController {


    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private GalleryRepository galleryRepository;

    private final UserService userService;

    @Autowired
    public GalleryController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/gallery/create")
    @PreAuthorize("isAuthenticated()")
    public String create(Model model) {

        List<Category> categories = this.categoryRepository.findAll();
        model.addAttribute("user", this.userService.userAuthentication());
        model.addAttribute("categories", categories);
        model.addAttribute("view", "gallery/create");
        return "base-layout";
    }

    @PostMapping("/gallery/create")
    @PreAuthorize("isAuthenticated()")
    public String createProcess(GalleryBindingModel galleryBindingModel) throws IOException {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        User userEntity = this.userRepository.findByEmail(user.getUsername());
        Category category = this.categoryRepository.findOne(galleryBindingModel.getCategoryId());
        HashSet<Tag> tags = this.findTagsFromString(galleryBindingModel.getTagString());


        Gallery galleryEntity = new Gallery(
                galleryBindingModel.getTitle(),
                galleryBindingModel.getInfo(),
                userEntity,
                category,
                tags

        );

        Gallery gallery = this.galleryRepository.saveAndFlush(galleryEntity);

        String encoded = Base64.getEncoder().encodeToString(galleryBindingModel.getImage().getBytes());
        gallery.setImage(encoded);

        this.galleryRepository.saveAndFlush(gallery);

        return "redirect:/gallery";
    }

    @GetMapping("/gallery/{id}")
    public String details(Model model, @PathVariable Integer id) {
        if (!this.galleryRepository.exists(id)) {
            return "redirect:/gallery";
        }
        boolean auth = false;
        Gallery gallery = this.galleryRepository.findOne(id);
        if(this.userService.userAuthentication().getId() != null){
            auth = true;
        }

        model.addAttribute("userAuth",auth);
        model.addAttribute("user", this.userService.userAuthentication());
        model.addAttribute("galleries", gallery);
        model.addAttribute("view", "gallery/details");

        return "base-layout";
    }

    @GetMapping("/gallery/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String edit(@PathVariable Integer id, Model model) {
        if (!this.galleryRepository.exists(id)) {
            return "redirect:/";
        }

        Gallery gallery = this.galleryRepository.findOne(id);


        if (!isUserAuthorOrAdmin(gallery)) {
            return "redirect:/gallery/" + id;
        }

        List<Category> categories = this.categoryRepository.findAll();

        model.addAttribute("user", this.userService.currentUser());
        model.addAttribute("view", "gallery/edit");
        model.addAttribute("gallery", gallery);
        model.addAttribute("categories", categories);


        return "base-layout";
    }

    @PostMapping("/gallery/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String editProcess(@PathVariable Integer id, GalleryBindingModel galleryBindingModel) {
        if (!this.galleryRepository.exists(id)) {
            return "redirect:/";
        }
        Gallery gallery = this.galleryRepository.findOne(id);

        if (!isUserAuthorOrAdmin(gallery)) {
            return "redirect:/gallery/" + id;
        }

        Category category = this.categoryRepository.findOne(galleryBindingModel.getCategoryId());


        gallery.setTitle(galleryBindingModel.getTitle());
        gallery.setInfo(galleryBindingModel.getInfo());
        gallery.setCategory(category);


        this.galleryRepository.saveAndFlush(gallery);
        return "redirect:/gallery/" + gallery.getId();
    }

    @GetMapping("/gallery/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String delete(Model model, @PathVariable Integer id) {

        if (!this.galleryRepository.exists(id)) {
            return "redirect:/gallery/";
        }

        Gallery gallery = this.galleryRepository.findOne(id);

        if (!isUserAuthorOrAdmin(gallery)) {
            return "redirect:/gallery/" + id;
        }
        model.addAttribute("user", this.userService.currentUser());
        model.addAttribute("galleries", gallery);
        model.addAttribute("view", "gallery/delete");

        return "base-layout";

    }


    @PostMapping("/gallery/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String deleteProcess(@PathVariable Integer id) {
        if (!this.galleryRepository.exists(id)) {
            return "redirect:/category/" + id;
        }

        Gallery gallery = this.galleryRepository.findOne(id);

        if (!isUserAuthorOrAdmin(gallery)) {
            return "redirect:/gallery/" + id;
        }

        this.galleryRepository.delete(gallery);

        return "redirect:/";
    }


    private HashSet<Tag> findTagsFromString(String tagString) {
        HashSet<Tag> tags = new HashSet<>();
        String[] tagNames = tagString.split(",\\s");

        for (String tagName : tagNames) {
            Tag currentTag = this.tagRepository.findByName(tagName);

            if (currentTag == null) {
                currentTag = new Tag(tagName);
                this.tagRepository.saveAndFlush(currentTag);
            }

            tags.add(currentTag);
        }
        return tags;
    }

    private boolean isUserAuthorOrAdmin(Gallery gallery) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        User userEntity = this.userRepository.findByEmail(user.getUsername());

        return userEntity.isAdmin() || userEntity.isAuthor(gallery);
    }
}
