package blog.controller;

import blog.bindingModel.UserBindingModel;
import blog.bindingModel.UserEditBindingModel;
import blog.entity.Gallery;
import blog.entity.Role;
import blog.entity.User;
import blog.repository.GalleryRepository;
import blog.repository.RoleRepository;
import blog.repository.UserRepository;
import blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Set;

@Controller
public class UserController {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;


    private final UserService userService;
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("view", "user/register");
        return "base-layout";
    }

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("view", "user/login");
        return "base-layout";
    }

    @PostMapping("/register")
    public String registerProcess(UserBindingModel userBindingModel) throws IOException {
        if (!userBindingModel.getPassword().equals(userBindingModel.getConfirmPassword())) {
            return "redirect:/register";
        }
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        User user = new User(
                userBindingModel.getEmail(),
                userBindingModel.getFirstName(),
                userBindingModel.getLastName(),
                bCryptPasswordEncoder.encode(userBindingModel.getPassword())
        );
        Role userRole = this.roleRepository.findByName("ROLE_USER");
        user.addRole(userRole);


        String encoded = Base64.getEncoder().encodeToString(userBindingModel.getProfilePicture().getBytes());
        user.setProfilePicture(encoded);

        this.userRepository.saveAndFlush(user);
        return "redirect:/login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    @GetMapping("/profile")
    @PreAuthorize("isAuthenticated()")
    public String profilePage(Model model) {
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        User user = this.userRepository.findByEmail(principal.getUsername());

        Set<Gallery> galleries = user.getGalleries();


        model.addAttribute("user",this.userService.currentUser());
        model.addAttribute("user", user);
        model.addAttribute("galleries", galleries);
        model.addAttribute("view", "user/profile");

        return "base-layout";
    }

    @GetMapping("/profile/{id}")
    @PreAuthorize("isAuthenticated()")
    public String publicProfile(@PathVariable Integer id,Model model){
        if (!this.userRepository.exists(id)) {
            return "redirect:/";
        }

        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();


        User user = this.userRepository.findOne(id);
        Set<Gallery> galleries = user.getGalleries();

        model.addAttribute("user",this.userService.userAuthentication());
        model.addAttribute("user",user);
        model.addAttribute("galleries", galleries);
        if (principal.getUsername().equals(user.getEmail())){
            model.addAttribute("view","user/profile");
        }else {
            model.addAttribute("view","user/public");
        }
        return "base-layout";
    }

    @GetMapping("/user/edit")
    public String edit(UserEditBindingModel userBindingModel, Model model) {
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        User user = this.userRepository.findByEmail(principal.getUsername());

        model.addAttribute("user",this.userService.currentUser());
        model.addAttribute("user", user);
        model.addAttribute("view", "user/edit");

        return "base-layout";
    }

    @PostMapping("/user/edit")
    public String editProcess(UserEditBindingModel userBindingModel) throws IOException {
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        User user = this.userRepository.findByEmail(principal.getUsername());

        if (!StringUtils.isEmpty(userBindingModel.getPassword())
                && !StringUtils.isEmpty(userBindingModel.getConfirmPassword())) {
            if (userBindingModel.getPassword().equals(userBindingModel.getConfirmPassword())) {
                BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

                user.setPassword(bCryptPasswordEncoder.encode(userBindingModel.getPassword()));
            }
        }

        if (!StringUtils.isEmpty(userBindingModel.getFirstName())) {
            user.setFirstName(userBindingModel.getFirstName());
        }
        user.setLastName(userBindingModel.getLastName());
        String encoded = Base64.getEncoder().encodeToString(userBindingModel.getProfilePicture().getBytes());
        user.setProfilePicture(encoded);

        this.userRepository.saveAndFlush(user);

        return "redirect:/profile";
    }


}
