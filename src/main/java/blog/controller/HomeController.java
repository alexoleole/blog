package blog.controller;

import blog.entity.Article;
import blog.entity.Category;
import blog.entity.Gallery;
import blog.repository.ArticleRepository;
import blog.repository.CategoryRepository;
import blog.repository.PictureRepository;
import blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Set;

@Controller
public class HomeController {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private PictureRepository pictureRepository;


    private final UserService userService;

    public HomeController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("user",this.userService.userAuthentication());
        model.addAttribute("view","home/search");

        return "base-layout";
    }

    @GetMapping("/article")
    public String article(Model model) {

        List<Article> articles = this.articleRepository.findAll();

        model.addAttribute("user",this.userService.userAuthentication());

        model.addAttribute("view", "home/list-articles");
        model.addAttribute("articles", articles);

        return "base-layout";
    }

    @GetMapping("/gallery")
    public String gallery(Model model) {

        List<Category> categories = this.categoryRepository.findAll();

        model.addAttribute("user",this.userService.userAuthentication());
        model.addAttribute("view", "gallery/index");
        model.addAttribute("categories", categories);

        return "base-layout";
    }

    @RequestMapping("/error/403")
    public String accessDenied(Model model){

        model.addAttribute("view","error/403");
        model.addAttribute("user",this.userService.userAuthentication());

        return "base-layout";
    }

    @GetMapping("/category/{id}")
    public String listGalleries(Model model, @PathVariable Integer id){
        if(!this.categoryRepository.exists(id)){
            return "redirect:/";
        }

        Category category = this.categoryRepository.findOne(id);
        Set<Gallery> galleries = category.getGalleries();

        /*List<Gallery> testGal = this.galleryRepository.findAllByCategoryId(id);*/

        model.addAttribute("user",this.userService.userAuthentication());
        model.addAttribute("galleries",galleries);
        model.addAttribute("category", category);
        model.addAttribute("view", "gallery/list-gallery");

        return "base-layout";
    }

}
