package blog.controller;

import blog.entity.Tag;
import blog.repository.TagRepository;
import blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class TagController {
    @Autowired
    private TagRepository tagRepository;

    private final UserService userService;
    @Autowired
    public TagController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/tag/gallery/{name}")
    public String filterPictures(Model model, @PathVariable String name) {
        Tag tag = this.tagRepository.findByName(name);

        if (tag == null) {
            return "redirect:/ ";
        }
        model.addAttribute("user",this.userService.userAuthentication());
        model.addAttribute("view", "tag/galleries");
        model.addAttribute("tag", tag);

        return "base-layout";
    }

    @GetMapping("/tag/articles/{name}")
    public String filterArticles(Model model, @PathVariable String name) {
        Tag tag = this.tagRepository.findByName(name);
        if (tag == null) {
            return "redirect:/ ";
        }
        model.addAttribute("user",this.userService.userAuthentication());
        model.addAttribute("view", "tag/articles");
        model.addAttribute("tag", tag);

        return "base-layout";
    }

}